const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send("This is test page");
});

app.get("/home", (req, res) => {
  res.send("This is home page");
});

app.listen(7000, () => {
  console.log("http://127.0.0.1:7000");
});
